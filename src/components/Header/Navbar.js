import React, { Component } from 'react';

import { Menu } from 'antd';

import logo from '../../assets/logo2.png';

const { SubMenu } = Menu;


export class Navbar extends Component {
    state = {
        current: 'home',
    };

    navClickHandler = e => {
        console.log('click ', e);
        this.setState({ current: e.key });
      };

    render() {
        return (
            <div>
                <div className="navbar-logo">
                    <img  
                        style={{
                            width: 200, 
                            height: 200, 
                            marginLeft: 'auto', 
                            marginRight:'auto', 
                            display: 'block',
                        }} 
                        src={logo}
                        alt={"MUSSOC Logo"}
                    />
                </div>
                <div>
                    <Menu onClick={this.navClickHandler} selectedKeys={[this.state.current]} mode="horizontal">
                        <SubMenu title={<span>Home</span>}>
                            <Menu.Item key="home">About Us</Menu.Item>
                            <Menu.Item key="execs">Meet Your Execs</Menu.Item>
                        </SubMenu>
                        <Menu.Item key="juniors">
                            Juniors
                        </Menu.Item>
                        <Menu.Item key="studio">
                            Music Studio
                        </Menu.Item>
                        <Menu.Item key="sponsorship">
                            Sponsorship
                        </Menu.Item>
                        <Menu.Item key="faq">
                            FAQs
                        </Menu.Item>
                        <Menu.Item key="gallery">
                            Gallery Exhibition
                        </Menu.Item>
                    </Menu>
                </div>
            </div>
        )
    }
}

export default Navbar
