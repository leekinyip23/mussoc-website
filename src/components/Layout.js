import React, { Component } from 'react';

import Navbar from './Header/Navbar'

export class Layout extends Component {
    render() {
        return (
            <div>
                <Navbar />
            </div>
        )
    }
}

export default Layout
